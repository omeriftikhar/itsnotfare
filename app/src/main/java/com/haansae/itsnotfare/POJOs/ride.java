package com.haansae.itsnotfare.POJOs;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

/**
 * Created by omer on 7/6/18.
 */

public class ride {

    private LatLng departureLatLng,destinationLatLng;
    private Map<String,LatLng> stopovers;
    private Map<String,String> passengers;
    private int numberOfPassengers;
    private String date;
    private Timer time;
    private boolean isCompleted,isPet,isSmoking,isAC,isMusic;
    private String carInfo;


    public ride() {

        this.departureLatLng =null;
        this.destinationLatLng = null;
        this.stopovers = null;
        this.passengers =null;
        this.numberOfPassengers = 0;
        this.date = null;
        this.time = null;
        this.isCompleted = false;
        this.isPet = false;
        this.isSmoking = false;
        this.isAC = false;
        this.isMusic = false;
        this.carInfo = null;

    }

    public ride(LatLng departureLatLng, LatLng destinationLatLng, Map<String, LatLng> stopovers, Map<String, String> passengers, int numberOfPassengers, String date, Timer time, boolean isCompleted, boolean isPet, boolean isSmoking, boolean isAC, boolean isMusic, String carInfo) {
        this.departureLatLng = departureLatLng;
        this.destinationLatLng = destinationLatLng;
        this.stopovers = stopovers;
        this.passengers = passengers;
        this.numberOfPassengers = numberOfPassengers;
        this.date = date;
        this.time = time;
        this.isCompleted = isCompleted;
        this.isPet = isPet;
        this.isSmoking = isSmoking;
        this.isAC = isAC;
        this.isMusic = isMusic;
        this.carInfo = carInfo;
    }


    public LatLng getDepartureLatLng() {
        return departureLatLng;
    }

    public void setDepartureLatLng(LatLng departureLatLng) {
        this.departureLatLng = departureLatLng;
    }

    public LatLng getDestinationLatLng() {
        return destinationLatLng;
    }

    public void setDestinationLatLng(LatLng destinationLatLng) {
        this.destinationLatLng = destinationLatLng;
    }

    public Map<String, LatLng> getStopovers() {
        return stopovers;
    }

    public void setStopovers(Map<String, LatLng> stopovers) {
        this.stopovers = stopovers;
    }

    public Map<String, String> getPassengers() {
        return passengers;
    }

    public void setPassengers(Map<String, String> passengers) {
        this.passengers = passengers;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Timer getTime() {
        return time;
    }

    public void setTime(Timer time) {
        this.time = time;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public boolean isPet() {
        return isPet;
    }

    public void setPet(boolean pet) {
        isPet = pet;
    }

    public boolean isSmoking() {
        return isSmoking;
    }

    public void setSmoking(boolean smoking) {
        isSmoking = smoking;
    }

    public boolean isAC() {
        return isAC;
    }

    public void setAC(boolean AC) {
        isAC = AC;
    }

    public boolean isMusic() {
        return isMusic;
    }

    public void setMusic(boolean music) {
        isMusic = music;
    }

    public String getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(String carInfo) {
        this.carInfo = carInfo;
    }
}
