package com.haansae.itsnotfare.OfferRideActivities;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.haansae.itsnotfare.R;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by omer on 7/5/18.
 */

public class CurrentLocationActivity extends FragmentActivity implements OnMapReadyCallback{

    public static final int LOCATION_REQUEST_CODE=100;

    ProgressBar mProgressBar;

    TextView mLocationText;
    AppCompatButton mCurrentLocationButton,confirmButton;

    RelativeLayout mapLayout;

    private GoogleMap mMap;

    int updateLocationFlag=1;

    //map
    SupportMapFragment mapFragment;

    //location vars
    LocationRequest mLocationRequest;
    Location mLocation;
    FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;

    //Marker
    Marker mCurrentMarker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //init views
        mLocationText=(TextView) findViewById(R.id.locationText);
        mProgressBar=(ProgressBar) findViewById(R.id.progressBar);
        mCurrentLocationButton=(AppCompatButton) findViewById(R.id.currentLocationButton);
        confirmButton=(AppCompatButton) findViewById(R.id.confirmButton);
        mapLayout=(RelativeLayout) findViewById(R.id.mapLayout);
        mapLayout.setVisibility(View.GONE);


        checkPermissions();


        //location setup
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        buildLocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                Log.i("loc_info","loc ok");

                startLocationUpdates();

            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e instanceof ResolvableApiException)
                {
                    try {
                        ResolvableApiException resolver=(ResolvableApiException) e;
                        resolver.startResolutionForResult(CurrentLocationActivity.this,0);
                    } catch (IntentSender.SendIntentException sendEx){}
                }
            }
        });


        //listeners/callbacks
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {

                    mLocation=location;

                    if(updateLocationFlag==1)
                    {
                        //to update location first time and to avoid moving camera uselessly
                        displayLocation();
                        updateLocationFlag++;
                    }
                    break;

                }

            }
        };

        mCurrentLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                displayLocation();
            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mLocation!=null)
                {/*
                    StartingPlaceActivity.myRideObject.setDepartureLatLng(new LatLng(mLocation.getLatitude(),mLocation.getLongitude()));*/

                    Intent destinationActivityIntent=new Intent(CurrentLocationActivity.this,DestinationActivity.class);
                    startActivity(destinationActivityIntent);
                }
            }
        });

    }

    private void checkPermissions()
    {
        if (ContextCompat.checkSelfPermission(CurrentLocationActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(CurrentLocationActivity.this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                //explanation
                /*Toast.makeText(getApplicationContext(),"Location permission is required for getting your location",Toast.LENGTH_LONG).show();*/
                ActivityCompat.requestPermissions(CurrentLocationActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);
            }
            else
            {
                ActivityCompat.requestPermissions(CurrentLocationActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(googleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(false);
        mMap.setIndoorEnabled(false);
        mMap.setBuildingsEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        startLocationUpdates();


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                showMarkerandSetLocation(latLng);
            }
        });

    }

    private void buildLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1*1000);
        mLocationRequest.setFastestInterval(1*1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    private void displayLocation()
    {
        final double longitude=mLocation.getLongitude();
        final double latitude=mLocation.getLatitude();

        if (mCurrentMarker!=null)
        {
            mCurrentMarker.remove();
        }

        Geocoder mGeocoder;
        List<Address> mAddresses;
        mGeocoder=new Geocoder(this, Locale.getDefault());
        try {
            mAddresses=mGeocoder.getFromLocation(latitude,longitude,1);

            if(mAddresses.size()==0)
            {
                Toast.makeText(getApplicationContext(),"No Location Information Available",Toast.LENGTH_SHORT).show();
                return;
            }

            String address=mAddresses.get(0).getAddressLine(0);

            mLocationText.setTextSize(12.0f);
            mLocationText.setText(address);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("Your Location"));
        mProgressBar.setVisibility(View.GONE);
        mapLayout.setVisibility(View.VISIBLE);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16.0f));

    }

    @Override
    protected void onPause() {
        super.onPause();

        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startLocationUpdates();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode)
        {
            case LOCATION_REQUEST_CODE:
            {

                Log.i("results", String.valueOf(grantResults[0]));

                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    startLocationUpdates();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Permission not granted\nUnable to show current location",Toast.LENGTH_LONG).show();
                }
            }
            break;
        }
    }


    private void showMarkerandSetLocation(LatLng mLatLng)
    {
        if (mCurrentMarker!=null)
        {
            mCurrentMarker.remove();
        }

        Geocoder mGeocoder;
        List<Address> mAddresses;
        mGeocoder=new Geocoder(this, Locale.getDefault());
        try {
            mAddresses=mGeocoder.getFromLocation(mLatLng.latitude,mLatLng.longitude,1);

            if(mAddresses.size()==0)
            {
                Toast.makeText(getApplicationContext(),"No Location Information Available",Toast.LENGTH_SHORT).show();
                return;
            }


           String address=mAddresses.get(0).getAddressLine(0);



            mLocationText.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }

        mCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLatLng.latitude,mLatLng.longitude))
                .title("Your Location"));
    }
}
