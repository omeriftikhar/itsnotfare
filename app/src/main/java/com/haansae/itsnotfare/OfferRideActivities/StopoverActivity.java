package com.haansae.itsnotfare.OfferRideActivities;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.haansae.itsnotfare.HelperClasses.RecyclerAdapter_Stopover;
import com.haansae.itsnotfare.R;

import java.util.ArrayList;
import java.util.List;

public class StopoverActivity extends AppCompatActivity {

    public static final int PLACE_PICKER_REQUEST=100;

    FloatingActionButton addStopoverButton;
    RecyclerView stopoversRecyclerView;
    RecyclerAdapter_Stopover mAdapter;
    CoordinatorLayout mCoordinatorLayout;
    AppCompatButton confirmStopoverButton;

    List<Place> stopovers;
    List<Place> selectedStopovers;
    Place place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopover);

        addStopoverButton=(FloatingActionButton) findViewById(R.id.addStopoverButton);
        stopoversRecyclerView=(RecyclerView) findViewById(R.id.stopoversRecyclerView);
        mCoordinatorLayout=(CoordinatorLayout) findViewById(R.id.coordinatorLayoutStopover);
        confirmStopoverButton=(AppCompatButton) findViewById(R.id.stopoverConfirmButton);


        stopovers=new ArrayList<Place>();






        //init Recycler view

        LinearLayoutManager mLinearLayoutManager=new LinearLayoutManager(this);
        stopoversRecyclerView.setLayoutManager(mLinearLayoutManager);

        mAdapter=new RecyclerAdapter_Stopover(getBaseContext(),stopovers);
        stopoversRecyclerView.setAdapter(mAdapter);






        //onclick
        ItemTouchHelper.SimpleCallback ItemTouchCallback=new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                mAdapter.removeItem(viewHolder.getAdapterPosition());

                Snackbar.make(mCoordinatorLayout,"Stopover deleted",Snackbar.LENGTH_LONG)
                        .setAction("Okay", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        }).show();
            }
        };


        new ItemTouchHelper(ItemTouchCallback).attachToRecyclerView(stopoversRecyclerView);


        addStopoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placePickerIntent();
            }
        });


        confirmStopoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedStopovers=new ArrayList<Place>();
                selectedStopovers.removeAll(mAdapter.getSelectedStopovers());
                selectedStopovers=mAdapter.getSelectedStopovers();


                Log.i("stopover","\n============================================================\n");

                for(int i=0; i<selectedStopovers.size();i++)
                {
                    Log.i("stopover",selectedStopovers.get(i).getAddress().toString()+"\n");
                }

                Log.i("stopover","\n============================================================\n");


                Intent dateTimeIntent=new Intent(StopoverActivity.this,DateActivity.class);
                startActivity(dateTimeIntent);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case PLACE_PICKER_REQUEST: {

                if (resultCode == RESULT_OK) {
                    place = PlacePicker.getPlace(this, data);

                    if(!(place==null))
                    {
                        if(mAdapter.stopovers.contains(place))
                        {
                            Snackbar.make(mCoordinatorLayout,"Stopover already added",Snackbar.LENGTH_LONG)
                                    .setAction("Okay", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                        }
                                    }).show();
                        }
                        else
                        {
                            stopovers.add(place);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }
    }


    private void placePickerIntent()
    {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
}
