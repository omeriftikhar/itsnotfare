package com.haansae.itsnotfare.OfferRideActivities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.PointerIcon;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import com.haansae.itsnotfare.R;

import java.util.Calendar;

public class TimeActivity extends AppCompatActivity {

    TimePicker mTimePicker;
    AppCompatButton confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        mTimePicker=(TimePicker) findViewById(R.id.clock);
        confirmButton=(AppCompatButton) findViewById(R.id.confirmTimeButton);

        if(mTimePicker.is24HourView())
            mTimePicker.setIs24HourView(false);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Toast.makeText(getApplicationContext(),
                    String.valueOf(mTimePicker.getHour())+":"+String.valueOf(mTimePicker.getMinute()),Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(),
                    String.valueOf(mTimePicker.getCurrentHour())+":"+String.valueOf(mTimePicker.getCurrentMinute()),Toast.LENGTH_LONG).show();
        }*/


        //setting time picker
        mTimePicker.setIs24HourView(false);
        Calendar myCalendar=Calendar.getInstance();


        //listeners
        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                Toast.makeText(getApplicationContext(),String.valueOf(i)+":"+String.valueOf(i1),Toast.LENGTH_LONG).show();
            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent passengerIntent=new Intent(TimeActivity.this,PassengerActivity.class);
                startActivity(passengerIntent);
            }
        });
    }
}
