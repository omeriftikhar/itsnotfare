package com.haansae.itsnotfare.OfferRideActivities;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.haansae.itsnotfare.MainActivity;
import com.haansae.itsnotfare.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class DestinationActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final int LOCATION_REQUEST_CODE=100;
    public static final int PLACE_PICKER_REQUEST=100;


    //Buttons and other view
    AppCompatButton destinationLocationButton,destinationLocationConfirmButton;
    TextView mEndingAddressText;
    RelativeLayout mapLayout,endingAddressTextLayout;

    //map and location vars
    private GoogleMap mMap;
    SupportMapFragment mapFragment;

    Marker mCurrentMarker;

    LocationRequest mLocationRequest;
    FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;

    Place place;

    double latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissions();
        setContentView(R.layout.activity_destination);




        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //init views
        destinationLocationButton=(AppCompatButton) findViewById(R.id.destinationLocationButton);
        destinationLocationConfirmButton=(AppCompatButton) findViewById(R.id.confirmDestinationLocationButton);
        mEndingAddressText=(TextView) findViewById(R.id.destination_address_text);
        mapLayout=(RelativeLayout) findViewById(R.id.mapLayout);
        endingAddressTextLayout=(RelativeLayout) findViewById(R.id.destination_address_text_layout);



        //location setup
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        buildLocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {


            }
        });


        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e instanceof ResolvableApiException)
                {
                    try {
                        ResolvableApiException resolver=(ResolvableApiException) e;
                        resolver.startResolutionForResult(DestinationActivity.this,0);
                    } catch (IntentSender.SendIntentException sendEx){}
                }
            }
        });







        //listeners
        destinationLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                placePickerIntent();

            }
        });


        destinationLocationConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!(place==null))
                {
                    StartingPlaceActivity.myRideObject.setDepartureLatLng(new LatLng(latitude,longitude));

                    Intent stopoverActivity = new Intent(DestinationActivity.this,
                            StopoverActivity.class);
                    startActivity(stopoverActivity);
                }
                else
                {
                    Snackbar.make(view,"Select a location",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    }).show();
                }

            }
        });

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

            }
        };




    }


    private void placePickerIntent()
    {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case PLACE_PICKER_REQUEST: {

                if (resultCode == RESULT_OK) {
                    place = PlacePicker.getPlace(this, data);

                    if(!(place==null))
                        populateMap();
                }
            }
        }
    }

    private void populateMap()
    {
        if (mCurrentMarker!=null)
        {
            mCurrentMarker.remove();
        }

        String address= (String) place.getAddress();

        if(address.isEmpty()|| address==null )
        {
            Toast.makeText(getApplicationContext(),"No Location Information Available",Toast.LENGTH_SHORT).show();
            return;
        }

        String textForScreen=address;
        if(address.length()>70)
        {
            textForScreen= (String) address.subSequence(0,70);
            textForScreen+="...";
        }

        mEndingAddressText.setText(textForScreen);

        mCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(place.getLatLng().latitude,place.getLatLng().longitude))
                .title("Your Destination"));
        mapLayout.setVisibility(View.VISIBLE);
        endingAddressTextLayout.setVisibility(View.VISIBLE);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(place.getLatLng().latitude,place.getLatLng().longitude),
                16.0f));


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode)
        {
            case LOCATION_REQUEST_CODE:
            {

                Log.i("results", String.valueOf(grantResults[0]));

                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    startLocationUpdates();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Permission not granted\nUnable to show location",Toast.LENGTH_LONG).show();

                    Intent backIntent=new Intent(DestinationActivity.this, MainActivity.class);
                    backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(backIntent);
                    finish();
                }
            }
            break;
        }
    }

    private void buildLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1*1000);
        mLocationRequest.setFastestInterval(1*1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startLocationUpdates();

    }


    private void checkPermissions()
    {
        if (ContextCompat.checkSelfPermission(DestinationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(DestinationActivity.this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                //explanation
                ActivityCompat.requestPermissions(DestinationActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);
            }
            else
            {
                ActivityCompat.requestPermissions(DestinationActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {


                latitude=latLng.latitude;
                longitude=latLng.longitude;

                showMarkerAndSetLocation(latLng);

                /*if(!(place==null))
                    populateMap();*/


            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMyLocationButtonClickListener(mListener);
    }


    GoogleMap.OnMyLocationButtonClickListener mListener=new GoogleMap.OnMyLocationButtonClickListener() {
        @Override
        public boolean onMyLocationButtonClick() {

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(mCurrentMarker.getPosition().latitude,mCurrentMarker.getPosition().longitude),
                    16.0f));

            return true;
        }
    };


    private void showMarkerAndSetLocation(LatLng mLatLng)
    {

        Geocoder mGeocoder;
        List<Address> mAddresses;
        mGeocoder=new Geocoder(this, Locale.getDefault());
        try {
            mAddresses=mGeocoder.getFromLocation(mLatLng.latitude,mLatLng.longitude,1);

            if(mAddresses.size()==0)
            {
                Toast.makeText(getApplicationContext(),"No Location Information Available",Toast.LENGTH_SHORT).show();
                return;
            }


            String address=mAddresses.get(0).getAddressLine(0);

            if(address.isEmpty()|| address==null )
            {
                Toast.makeText(getApplicationContext(),"No Location Information Available",Toast.LENGTH_SHORT).show();
                return;
            }

            String textForScreen=address;
            if(address.length()>70)
            {
                textForScreen= (String) address.subSequence(0,70);
                textForScreen+="...";
            }



            mEndingAddressText.setText(textForScreen);

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mCurrentMarker!=null)
        {
            mCurrentMarker.remove();
        }


        mCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLatLng.latitude,mLatLng.longitude))
                .title("Your Destination"));


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(mLatLng.latitude,mLatLng.longitude),
                16.0f));
    }
}
