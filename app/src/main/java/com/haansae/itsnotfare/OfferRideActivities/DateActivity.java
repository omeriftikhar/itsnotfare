package com.haansae.itsnotfare.OfferRideActivities;

import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.haansae.itsnotfare.HelperClasses.TimePickerFragment;
import com.haansae.itsnotfare.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateActivity extends AppCompatActivity {

    CalendarView mCalendar;
    AppCompatButton confirmButton;
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        //init views
        mCalendar=(CalendarView) findViewById(R.id.calendar);
        confirmButton=(AppCompatButton) findViewById(R.id.comfirmDateButton);
        date=null;
        //to disable previous dates
        mCalendar.setMinDate(System.currentTimeMillis()-1000);
        //Monday as first day
        mCalendar.setFirstDayOfWeek(1);
        Calendar myCalendar=Calendar.getInstance();
        //Set current date as initial date
        date=getDate(myCalendar.get(Calendar.DAY_OF_MONTH),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.YEAR));



        //listeners
        mCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {

                date=getDate(year,month,day);
                Toast.makeText(getApplicationContext(),date,Toast.LENGTH_LONG).show();

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StartingPlaceActivity.myRideObject.setDate(date);
                Toast.makeText(getApplicationContext(),date,Toast.LENGTH_LONG).show();

                Intent timeIntent=new Intent(DateActivity.this,TimeActivity.class);
                startActivity(timeIntent);

            }
        });


    }

    private String getDate(int year,int month,int day)
    {
        String mYear=String.valueOf(year);
        String mMonth=String.valueOf(month+1);
        String mDay=String.valueOf(day);

        date=mDay+"-"+mMonth+"-"+mYear;

        return date;
    }
}
