package com.haansae.itsnotfare;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class phoneNumberActivity extends AppCompatActivity {


    AppCompatEditText phoneNumberTextBox;
    TextInputLayout phoneNumberTIP;
    RelativeLayout screenLayout;
    AppCompatButton phoneVerificationButton;


    //Firebase
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth=FirebaseAuth.getInstance();

        //if user is already logged in
        if (mAuth.getCurrentUser()!=null)
        {
            Intent mainActivityIntent=new Intent(phoneNumberActivity.this,MainActivity.class);
            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainActivityIntent);
        }
        setContentView(R.layout.activity_phone_number);




        //views
        phoneNumberTextBox=(AppCompatEditText) findViewById(R.id.phoneNumberTextBox);
        phoneNumberTIP= (TextInputLayout) findViewById(R.id.textInputLayout);
        screenLayout=(RelativeLayout) findViewById(R.id.relativeLayoutphoneNumActivity);
        phoneVerificationButton=(AppCompatButton) findViewById(R.id.phoneVerificationButton);

        //listeners

        screenLayout.setOnClickListener(null);

        phoneNumberTextBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String number=charSequence.toString();

                phoneNumberVerification(number);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                String number=editable.toString();

                phoneNumberVerification(number);

            }
        });

        phoneNumberTextBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {

                String number=phoneNumberTextBox.getText().toString();


                if(!hasFocus)
                {
                    InputMethodManager keyboard = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(view.getWindowToken(),0);

                    phoneNumberVerification(number);
                }

            }
        });


        phoneVerificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String number=phoneNumberTextBox.getText().toString();
                if(!phoneNumberTIP.isErrorEnabled())
                {
                    Intent phoneVerificationIntent=new Intent(phoneNumberActivity.this,phoneNumberVerification.class);
                    phoneVerificationIntent.putExtra("number",number);
                    startActivity(phoneVerificationIntent);
                }

                phoneNumberVerification(number);
            }
        });
    }


    private void phoneNumberVerification(String number)
    {
        if ((number.length()>2)&&(!number.startsWith("+92")))
        {
            phoneNumberTIP.setErrorEnabled(true);
            phoneNumberTIP.setError("Write in format +923xxxxxxxxxx");
        }
        else if(number.isEmpty())
        {
            phoneNumberTIP.setErrorEnabled(true);
            phoneNumberTIP.setError("Enter Phone Number");
        }
        else if(number.isEmpty()&&(!phoneNumberTextBox.isFocused()))
        {

            phoneNumberTIP.setErrorEnabled(true);
            phoneNumberTIP.setError("Enter Phone Number");

        }
        else if(number.length()>13)
        {
            phoneNumberTIP.setErrorEnabled(true);
            phoneNumberTIP.setError("Number must be of 13");
        }
        else
        {
            phoneNumberTIP.setErrorEnabled(false);
        }
    }
}
