package com.haansae.itsnotfare;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.haansae.itsnotfare.HelperClasses.IntroScreenManager;

public class WelcomeActivity extends AppCompatActivity {


    ViewPager mViewPager;
    LinearLayout dotsLayout;
    TextView[] dots;
    int[] layouts;
    AppCompatButton skipButton,nextButton;
    IntroScreenManager mIntroManager;
    MyViewPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //if app is getting started first time then show intro slider else not
        mIntroManager=new IntroScreenManager(this);
        if (!mIntroManager.isFirstTimeLaunched())
        {
            skipIntro();
        }

        //Making status bar transparent

        if(Build.VERSION.SDK_INT>=21)
        {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);

        //inits
        mViewPager=(ViewPager) findViewById(R.id.introScreenSlider);
        dotsLayout=(LinearLayout) findViewById(R.id.dotsLayout);
        nextButton=(AppCompatButton) findViewById(R.id.btn_next);
        skipButton=(AppCompatButton) findViewById(R.id.btn_skip);


        //getting layouts of all intro slider screens
        layouts=new int[]
                {
                        R.layout.layout_intro_screen1,
                        R.layout.layout_intro_screen2,
                        R.layout.layout_intro_screen3,
                        R.layout.layout_intro_screen4
                };

        addBottomDots(0);

        changeStatusBarColor();


        mAdapter=new MyViewPagerAdapter();
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(viewPageChangeListener);

        //listeners

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                skipIntro();

            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    mViewPager.setCurrentItem(current);
                } else {
                    skipIntro();
                }
            }
        });







    }

    private void skipIntro() {
        mIntroManager.setFirstTimeLaunch(false);
        Intent skipIntroIntent=new Intent(WelcomeActivity.this,phoneNumberActivity.class);
        startActivity(skipIntroIntent);
        finish();
    }

    private void addBottomDots(int currentPage) {

        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }


    //viewpager listener
    ViewPager.OnPageChangeListener viewPageChangeListener=new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            addBottomDots(position);

            if(position==layouts.length-1)
            {
                nextButton.setText(R.string.proceed);
                skipButton.setVisibility(View.GONE);
            }
            else
            {
                nextButton.setText(R.string.next);
                skipButton.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }



    //View pager adapter
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
