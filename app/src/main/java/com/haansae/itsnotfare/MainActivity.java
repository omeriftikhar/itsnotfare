package com.haansae.itsnotfare;


import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.haansae.itsnotfare.Fragments.AlertsFragment;
import com.haansae.itsnotfare.Fragments.OfferRideFragment;
import com.haansae.itsnotfare.Fragments.ProfileFragment;
import com.haansae.itsnotfare.Fragments.SearchRideFragment;
import com.haansae.itsnotfare.HelperClasses.BottomNavigationViewHelper;
import com.haansae.itsnotfare.OfferRideActivities.StartingPlaceActivity;


public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{


    BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBottomNavigationView=findViewById(R.id.bottomNavigationView);
        BottomNavigationViewHelper.disableShiftMode(mBottomNavigationView);


        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        //first time this screens comes then we will load first fragment(any fragment can be loaded as required)
        loadFragment(new OfferRideFragment());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        android.support.v4.app.Fragment fragment=null;

        switch (item.getItemId())
        {
            case R.id.offerRide:
                fragment=new OfferRideFragment();
                break;

            case R.id.searchRide:
                fragment=new SearchRideFragment();
                break;

            case R.id.notifications:
                fragment=new AlertsFragment();
                break;

            case R.id.profile:
                fragment=new ProfileFragment();
                break;
        }

        return loadFragment(fragment);
    }


    private boolean loadFragment(android.support.v4.app.Fragment mFragment)
    {
        if(mFragment!=null)
        {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentsContainer,mFragment)
                    .commit();
            return true;
        }

        return false;

    }


    /*@Override
    public void onBackPressed() {

        Toast.makeText(getApplicationContext(),String.valueOf(getSupportFragmentManager().getBackStackEntryCount()),Toast.LENGTH_SHORT).show();

        if(getSupportFragmentManager().getBackStackEntryCount()>1)
        {
            getSupportFragmentManager().popBackStack();
        }
        else
        {
            super.onBackPressed();
            finish();
        }
    }*/
}
