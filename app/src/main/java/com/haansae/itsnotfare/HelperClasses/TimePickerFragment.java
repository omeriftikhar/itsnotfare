package com.haansae.itsnotfare.HelperClasses;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import java.util.Calendar;

/**
 * Created by omer on 7/8/18.
 */

public class TimePickerFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Calendar mCalendar=Calendar.getInstance();
        int hour=mCalendar.get(Calendar.HOUR_OF_DAY);
        int minute=mCalendar.get(Calendar.MINUTE);


        return new TimePickerDialog(getActivity(),
                (TimePickerDialog.OnTimeSetListener) getActivity(),
                hour,
                minute,
                DateFormat.is24HourFormat(getActivity()));
    }
}
