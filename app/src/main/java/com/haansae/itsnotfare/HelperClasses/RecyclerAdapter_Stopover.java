package com.haansae.itsnotfare.HelperClasses;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.google.android.gms.location.places.Place;
import com.haansae.itsnotfare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omer on 7/7/18.
 */

public class RecyclerAdapter_Stopover extends RecyclerView.Adapter<ViewHolder_Stopover> {

    public List<Place> stopovers;
    Context mContext;

    public RecyclerAdapter_Stopover(Context context,List<Place> stopovers) {
        this.stopovers = stopovers;
        this.mContext=context;
    }

    @Override
    public ViewHolder_Stopover onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_stopover_item,parent,false);

        return new ViewHolder_Stopover(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder_Stopover holder, final int position) {

        final Place place=stopovers.get(position);

        if(place.getName().toString().contains("\u00b0"))
        {
            holder.stopoverName.setVisibility(View.GONE);
            holder.stopoverAddress.setTextSize(16.0f);
            holder.stopoverAddress.setTypeface(Typeface.DEFAULT_BOLD);
        }


        holder.stopoverName.setText(place.getName());
        holder.stopoverAddress.setText(place.getAddress());




    }

    @Override
    public int getItemCount() {
        return stopovers.size();
    }

    public List<Place> getSelectedStopovers()
    {
        return stopovers;
    }

    public void removeItem(int pos)
    {
        stopovers.remove(pos);
        notifyItemRemoved(pos);
    }
}
