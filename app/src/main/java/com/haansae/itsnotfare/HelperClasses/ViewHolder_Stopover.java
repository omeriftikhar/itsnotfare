package com.haansae.itsnotfare.HelperClasses;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.haansae.itsnotfare.R;

/**
 * Created by omer on 7/7/18.
 */

public class ViewHolder_Stopover extends RecyclerView.ViewHolder {


    public TextView stopoverName,stopoverAddress;

     public ViewHolder_Stopover(View itemView) {
        super(itemView);


        stopoverName=(TextView) itemView.findViewById(R.id.stopoverName);
        stopoverAddress=(TextView) itemView.findViewById(R.id.stopoverAddress);
    }
}
