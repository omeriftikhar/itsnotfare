package com.haansae.itsnotfare.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by omer on 7/3/18.
 */

public class IntroScreenManager {

    SharedPreferences myPreference;
    SharedPreferences.Editor editor;
    Context mContext;


    int PRIVATE_MODE=0;

    private static final String preferenceName="IntroScreenSliderPreference";
    private static final String isFirstTimeLaunched="isFirstTimeLaunched";

    public IntroScreenManager(Context context)
    {
        this.mContext=context;
        myPreference=mContext.getSharedPreferences(preferenceName,PRIVATE_MODE);
        editor=myPreference.edit();
    }


    public void setFirstTimeLaunch(boolean isFirstTimeLaunced)
    {
        editor.putBoolean(isFirstTimeLaunched,isFirstTimeLaunced);
        editor.commit();
    }


    public boolean isFirstTimeLaunched()
    {
        return myPreference.getBoolean(isFirstTimeLaunched,true);
    }
}
