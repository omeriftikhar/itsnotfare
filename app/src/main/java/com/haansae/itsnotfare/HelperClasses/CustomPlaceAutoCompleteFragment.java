package com.haansae.itsnotfare.HelperClasses;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.haansae.itsnotfare.OfferRideActivities.DestinationActivity;
import com.haansae.itsnotfare.R;


/**
 * Created by omer on 7/6/18.
 */

public class CustomPlaceAutoCompleteFragment extends PlaceAutocompleteFragment {

    public static Place place;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override public void onPlaceSelected(Place place) {

                CustomPlaceAutoCompleteFragment.this.place = place;
            }

            @Override public void onError(Status status) {

            }
        });
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @Override public void onActivityResult(int i, int i1, Intent intent) {
        super.onActivityResult(i, i1, intent);
        ((AppCompatEditText) getView()
                .findViewById(R.id.place_autocomplete_search_input)).setText(place.getAddress());
    }

}
