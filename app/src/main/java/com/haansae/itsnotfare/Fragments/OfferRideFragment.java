package com.haansae.itsnotfare.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.haansae.itsnotfare.OfferRideActivities.StartingPlaceActivity;
import com.haansae.itsnotfare.R;

/**
 * Created by omer on 7/4/18.
 */

public class OfferRideFragment extends Fragment {


    AppCompatButton offerRideButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_offer_ride,null);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        offerRideButton=(AppCompatButton) getView().findViewById(R.id.offerRideButton);


        offerRideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startingPlaceIntent=new Intent(getActivity(), StartingPlaceActivity.class);
                startActivity(startingPlaceIntent);
            }
        });
    }
}
