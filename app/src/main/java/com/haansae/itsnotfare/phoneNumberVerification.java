package com.haansae.itsnotfare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class phoneNumberVerification extends AppCompatActivity {

    String mVerificationId;
    String phoneNumber;

    AppCompatEditText mVerificationCodeBox;
    AppCompatButton codeVerificationButton;
    TextInputLayout codeVerificationBoxTIP;


    //Firebase
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_verification);


        //inits
        mAuth=FirebaseAuth.getInstance();
        mVerificationCodeBox=(AppCompatEditText) findViewById(R.id.verificationCodeTextBox);
        codeVerificationButton=(AppCompatButton) findViewById(R.id.codeVerificationButton);
        codeVerificationBoxTIP=(TextInputLayout) findViewById(R.id.textInputLayout);


        //getting phone number entered by user
        if(getIntent()!=null)
        {
            phoneNumber=getIntent().getStringExtra("number");

        }

        sendVerificationCode(phoneNumber);


        //listeners

        codeVerificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String code = mVerificationCodeBox.getText().toString().trim();

                if (code.isEmpty() || code.length() < 6) {
                    codeVerificationBoxTIP.setError("Enter valid code");
                    codeVerificationBoxTIP.requestFocus();
                    return;
                }

                verifyVerificationCode(code);
            }
        });
    }


    private void sendVerificationCode(String number)
    {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallback);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback=new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            String smsCode=phoneAuthCredential.getSmsCode();

            if(smsCode!=null)
            {
                mVerificationCodeBox.setText(smsCode);

                verifyVerificationCode(smsCode);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            Toast.makeText(phoneNumberVerification.this, e.getMessage(), Toast.LENGTH_LONG).show();

        }


        @Override
        public void onCodeSent(String originalCode, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(originalCode, forceResendingToken);

            mVerificationId=originalCode;
        }
    };

    private void verifyVerificationCode(String code) {

        PhoneAuthCredential credentials=PhoneAuthProvider.getCredential(mVerificationId,code);


        //sign-in user

        signInWithPhoneAuthCredentials(credentials);
    }

    private void signInWithPhoneAuthCredentials(PhoneAuthCredential credentials) {

        mAuth.signInWithCredential(credentials)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful())
                        {
                            Intent mainActivityIntent=new Intent(phoneNumberVerification.this,MainActivity.class);
                            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mainActivityIntent);
                        }
                        else
                        {

                            Toast.makeText(getApplicationContext(),"Login failed.", Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }
}
